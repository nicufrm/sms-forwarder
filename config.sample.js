module.exports = {
    cachePath: './sms-cache.json', //path to cache file or false if you don't want to save to disk
    log: true,
    email: {
        service:'Gmail',
        auth: {
            user : "name@example.com",
            pass : "gmail_password_goes_here",
        },
    },
    modem: {
        ip: '192.168.8.1',
        user: 'admin',
        pass: 'admin'
    },
    message:{
        from: 'name@example.com',
        to: 'your.email@example.com',
    },
}
