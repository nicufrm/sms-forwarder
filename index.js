#!/usr/bin/env node

const config  = require('./config');
const mailBodyTemplate  = require('./mail-body');
const getMessages  = require('./modem');
//const getMessages  = require('./mockmodem');
const crypto = require('crypto');
const nodemailer = require('nodemailer');
const md5 = (text) => crypto.createHash('md5').update(text).digest('hex');
const mailTransport = nodemailer.createTransport(config.email);
const MessageCache = require('./cache');
const messageCache = new MessageCache(config.cachePath);

const {ip, user, pass} = config.modem;

const newMessage = (message, hash) => {
    const from = message.Phone._text;
    const date = message.Date._text;
    const body = message.Content._text;

    const mailBody = mailBodyTemplate(from, date, body);

    mailTransport.sendMail({
        from: `"SMS ${from}" <${config.message.from}>`,
        to: config.message.to,
        subject: `${from}: ${body}`,
        text: mailBody
    });
}

const logMessage = (message, info) => {
    if(!config.log) return;

    const from = message.Phone._text;
    const date = message.Date._text;
    const body = message.Content._text;

    console.log(
    info + '\n',
    'From: ', from,
    'Date: ', date,
    'Body: \n', body,
    '\n-----------------\n');
}

setInterval(() => {
    getMessages(ip, user, pass, (messages) => {
        let messageList = messages.response.Messages.Message;
        if (messageList) {
            if (!Array.isArray(messageList)) {
                messageList = [messageList];
            }
            for(let message of messageList){
                const hash = md5(message.Date._text + message.Phone._text);
                if(!messageCache.exists(hash)) {
                    newMessage(message, hash);
                    logMessage(message, 'NEW MESSAGE');
                    messageCache.add(hash, 1);
                    messageCache.save();
                } else {
                    logMessage(message, 'CACHED');
                }
            }
        }
    })
}, 5000);

