const fs = require('fs');

class messageCache {
  constructor(path) {
    this.cache = {};

    if (path) {
      this.path = path;
      this.load();
    }
  }

  load() {
    fs.readFile(this.path, (err, data) => {
      if(!err){
        try {
          this.cache = JSON.parse(data);
        } catch (error) {}
      }
    });
  }

  save(callback) {
    if(!this.path) return;

    const data = JSON.stringify(this.cache, null, 2);
    fs.writeFile(this.path, data, 'utf8', ()=>{
      if (callback) callback();
    });
  }

  exists(hash) {
    return this.cache.hasOwnProperty(hash);
  }

  add(hash, data) {
    this.cache[hash] = data;
  }
}

module.exports = messageCache;