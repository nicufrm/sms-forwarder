# sms-forwarder

A script that forwards text messages from a Huawei 4g hotspot to an email address.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine

### Prerequisites

```
nodejs and npm
```

### Installing

```
git clone https://gitlab.com/nicufrm/sms-forwarder.git
npm install
```
Also make sure that the path for the cache file is writable.

### Configuration

Copy `config.sample.js` to `config.js` and edit the credentials and settings as you see fit

### Running

```
./index.js
```

### Install as a systemd service
To add the service:
```
cp sms.service /etc/systemd/system/
```

To run it automatically at startup:
```
systemctl enable sms
```

To start the service:
```
systemctl start sms
```

To stop the service:
```
systemctl stop sms
```

To see the log:
```
journalctl -fu sms
```
## Authors

* **Nicu Farmache**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
